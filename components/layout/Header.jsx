import {useState} from "react";
import {FaSearch, FaShoppingCart} from "react-icons/fa";
import Logo from "../ui/Logo";
import Search from "../ui/Search";
import {GiHamburgerMenu} from "react-icons/gi";
import Link from "next/link";
import {useSelector} from "react-redux";

const Header = () => {
  const [isSearchModal, setIsSearchModal] = useState(false);
  const [isMenuModal, setIsMenuModal] = useState(false);
  const cart = useSelector((state) => state.cart);
  return (
    <div
      className={`h-[5.5rem] z-50 relative w-full bg-secondary`}
    >
      <div className="container mx-auto text-white flex justify-between items-center h-full">
        <Logo />
        <div className="flex gap-x-4 items-center">
          <Link href="/">
            <span className="relative">
              <FaShoppingCart
                className={`hover:text-primary transition-all cursor-pointer`}
              />
              <span className="w-4 h-4 text-xs grid place-content-center rounded-full bg-primary absolute -top-2 -right-3 text-black font-bold">
                {cart.products.length === 0 ? "0" : cart.products.length}
              </span>
            </span>
          </Link>
          <button onClick={() => setIsSearchModal(true)}>
            <FaSearch className="hover:text-primary transition-all cursor-pointer" />
          </button>
          <a href="#" className="md:inline-block hidden sm">
            <button className="btn-primary">Order Online</button>
          </a>
          <button
            className="sm:hidden inline-block"
            onClick={() => setIsMenuModal(true)}
          >
            <GiHamburgerMenu className="text-xl hover:text-primary transition-all" />
          </button>
        </div>
      </div>
      {isSearchModal && <Search setIsSearchModal={setIsSearchModal} />}
    </div>
  );
};

export default Header;
