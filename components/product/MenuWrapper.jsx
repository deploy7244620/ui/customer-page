import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import BaseService from "../../pages/api";
import Title from "../ui/Title";
import MenuItem from "./MenuItem";

const MenuWrapper = () => {
    const productEndpoint = process.env.NEXT_PUBLIC_API_URL_1;
    const orderEndpoint = process.env.NEXT_PUBLIC_API_URL;
    const [active, setActive] = useState(0);
    const [filter, setFilter] = useState([]);
    const [productLimit, setProductLimit] = useState(3);
    const router = useRouter();
    const [categoryList, setCategoryList] = useState([]);
    const [productList, setProductList] = useState([]);
    const baseService = new BaseService();

    useEffect(() => {
        baseService.getCartNumber(orderEndpoint, "/order-details/cart").then((response) => {

        });
        baseService
            .get(productEndpoint, "/categories", {
                restaurantId: router.query["restaurantId"],
            })
            .then((response) => {
                setCategoryList(response["result"]["data"]);
                getMenuByCate(response["result"]["data"][0]);
            });
    }, []);

    const getMenuByCate = (cate) => {
        baseService
            .get(productEndpoint, "/menu", {
                restaurantId: router.query["restaurantId"],
                categoriesId: cate["_id"],
            })
            .then((response) => {
                setProductList(response["result"]["data"]);
            });
    };
    return (
        <div className="container mx-auto  mb-16">
            <div className="flex flex-col items-center w-full">
                <Title addClass="text-[40px]">Menu</Title>
                <div className="mt-10">
                    {categoryList?.map((category, index) => (
                            <button
                                className={`px-6 py-2 ${
                                    index === active && "bg-secondary text-white "
                                } ml-4 rounded-3xl `}
                                key={category._id}
                                onClick={() => {
                                    setActive(index);
                                    setProductLimit(3);
                                    getMenuByCate(category);
                                }}
                            >
                                {category.name}
                            </button>
                        ))}
                </div>
            </div>
            <div className="flex justify-center">
                {/* {filter.length > 0 &&
          filter
            .slice(0, productLimit)
            .map((product) => <MenuItem key={product._id} product={product} />)} */}
                <List
                    sx={{width: "100%", maxWidth: 360, bgcolor: "background.paper"}}
                >
                    {productList?.map((product) => (
                            <ListItem alignItems="center" key={product._id}>
                                <MenuItem product={product}></MenuItem>
                            </ListItem>
                        ))}
                </List>
            </div>
            <div className="flex items-center justify-center my-8">
                <button
                    className="btn-primary"
                    onClick={() => setProductLimit(productLimit + 3)}
                >
                    Xem thêm
                </button>
            </div>
        </div>
    );
};

export default MenuWrapper;
