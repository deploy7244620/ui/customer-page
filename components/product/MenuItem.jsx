import AddIcon from "@mui/icons-material/Add";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Checkbox from "@mui/material/Checkbox";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import {useState} from "react";
import {NumericFormat} from "react-number-format";
import {useDispatch, useSelector} from "react-redux";
import {addProduct} from "../../redux/cartSlice";
import {NumberInput} from "../form/NumberInput";
import BaseService from "../../pages/api";

const MenuItem = ({ product }) => {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const [activeDetail, setActiveDetail] = useState(false);
  const baseService = new BaseService();
  // console.log(product);
  const findCart = cart.products.find((item) => item._id === product._id);
  const [quantity, setQuantity] = useState(1);
  const addToCart = () => {
    dispatch(
      addProduct({
        ...product,
        extras: [],
        price: product.prices[0],
        quantity: 1,
        foodQuantity: 1,
      })
    );
  };
  const handleClose = () => {
      baseService.addCart
    setActiveDetail(false);
    setQuantity(1);
  };
  return (
    // <div className="bg-secondary rounded-3xl relative">
    //   <div className="w-full  bg-[#f1f2f3] h-[210px] grid place-content-center rounded-bl-[46px] rounded-tl-2xl rounded-tr-2xl ">
    //     <Link href={`/product/${product._id}`}>
    //       <div className="relative w-36 h-36 hover:scale-110 transition-all">
    //         <Image
    //           src={product.img}
    //           alt=""
    //           layout="fill"
    //           className="rounded-full"
    //         />
    //       </div>
    //     </Link>
    //   </div>
    //   <div className="p-[25px] text-white ">
    //     <h4 className="text-xl font-semibold mb-3 ">{product.title}</h4>
    //     <p className="text-[15px]">{product.desc}</p>
    //     <div className="flex justify-between items-center mt-4">
    //       <span>${product.prices[0]}</span>
    //       <button
    //         className="btn-primary !w-10 !h-10 !rounded-full !p-0 grid place-content-center absolute bottom-4 right-5"
    //         disabled={findCart}
    //         onClick={addToCart}
    //       >
    //         <RiShoppingCart2Fill />
    //       </button>
    //     </div>
    //   </div>
    // </div>
    <Card sx={{ minWidth: 325, maxWidth: 345 }}>
      <CardMedia
        sx={{ height: 140 }}
        image={product.imageUrl[0]}
        title={product.name}
        onClick={() => {
          setActiveDetail(true);
        }}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {product.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {product.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={6}>
            <IconButton
              color="primary"
              aria-label="add to shopping cart"
              onClick={() => {
                setActiveDetail(true);
              }}
            >
              <AddIcon />
            </IconButton>
          </Grid>
          <Grid item xs={6} sx={{ textAlign: "end" }}>
            {/* <Typography align="center">{product.price} VND</Typography>    */}
            <div>
              <NumericFormat
                displayType="text"
                value={product.price}
                allowLeadingZeros
                thousandSeparator=","
              />{" "}
              VND
            </div>
          </Grid>
        </Grid>
      </CardActions>
      <Dialog
        open={activeDetail}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth
        maxWidth="xs"
      >
        <DialogTitle id="alert-dialog-title">{product.name}</DialogTitle>
        <DialogContent>
          <div className="w-full relative pt-[50%]">
            <Image
              src={product.imageUrl[0]}
              alt="profile"
              objectFit="cover"
              layout="fill"
              className="w-full h-full top-0 left-0 object-cover rounded-2xl"
            />
          </div>
          <FormGroup>
            {product &&
              product.addOn &&
              product["addOn"].map((addOn, i) => (
                <FormControlLabel
                  control={<Checkbox />}
                  label={addOn}
                  key={i}
                />
              ))}
            <NumberInput
              onChange={(event, val) => {
                setQuantity(val);
              }}
              min={1}
              max={99}
            />
          </FormGroup>
          <Box sx={{ fontWeight: "bold", m: 1 }}>
            Tổng:{" "}
            <NumericFormat
              displayType="text"
              value={quantity * product.price}
              allowLeadingZeros
              thousandSeparator=","
            />{" "}
            VND
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Thêm
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
};

export default MenuItem;
