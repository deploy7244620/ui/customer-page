/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  images: {
    domains: ["storage-service.hanzomaster.me"],
  },
};

module.exports = nextConfig;
