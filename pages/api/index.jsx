import axios from 'axios';

const APP_URL = process.env.NEXT_PUBLIC_API_URL;
export default class BaseService {
    get(endpoint ,url, params = {}, headers = {}) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'get',
                url: endpoint + url,
                headers: {
                    'Content-Type': 'application/json',
                    ...headers
                },
                params: params
            })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch((error) => {
                    this._errorHandler(reject, error);
                });
        });
    }
    getCartNumber(endpoint, url, params = {}, headers = {}, body) {
        console.log('endpoint', endpoint);
        console.log('params', params);
        console.log('headers', headers);
        console.log('url', url);
        return new Promise((resolve, reject) => {

        });
// return new Promise((resolve, reject) => {
//             axios({
//                 method: 'post',
//                 url: APP_URL + '/order-details/cart',
//                 headers: {
//                     'Content-Type': 'application/json',
//                     ...headers
//                 },
//                 params: params
//             })
//                 .then(function (response) {
//                     resolve(response.data);
//                 })
//                 .catch((error) => {
//                     this._errorHandler(reject, error);
//                 });
//         });
    }
    post(url, data = {}, headers = {}) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: APP_URL + url,
                headers: {
                    'Content-Type': 'application/json',
                    ...headers
                },
                data: data
            })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch((error) => {
                    this._errorHandler(reject, error);
                });
        });
    }
    put(url, data = {}, headers = {}) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'put',
                url: APP_URL + url,
                headers: {
                    'Content-Type': 'application/json',
                    ...headers
                },
                data: data
            })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch((error) => {
                    this._errorHandler(reject, error);
                });
        });
    }
    delete(url, data = {}, headers = {}) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'delete',
                url: APP_URL + url,
                headers: {
                    'Content-Type': 'application/json',
                    ...headers
                },
                data: data
            })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch((error) => {
                    this._errorHandler(reject, error);
                });
        });
    }
    postFormData(url, data = {}, headers = {}) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: APP_URL + url,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    ...headers
                },
                data: data
            })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch((error) => {
                    this._errorHandler(reject, error);
                });
        });
    }
    _errorHandler(reject, err) {
        if (err.response && err.response.status === 401) {
            // Unauthorized (session timeout)
            localStorage.clear();
            window.location.href = '/auth/login';
        }
        return reject(err);
    }
}
