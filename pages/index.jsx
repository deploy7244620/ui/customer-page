import Head from "next/head";
import Home from "./home";
import axios from "axios";
import { useRouter } from "next/router";
import BaseService from "./api";
import { useEffect, useState } from "react";

export default function Index(props) {
  useEffect(() => {
    if (props.orderId) {
      window.history.pushState( {} , 'orderId', `?orderId=${props.orderId}&restaurantId=${props.restaurantId}` );
    }
  }, [props.orderId]);
  return (
    <div className="">
      <Head>
        <title>Feane</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
      </Head>
      <Home props/>
    </div>
  );
}

export const getServerSideProps = async (context) => {
  // const res = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/categories`);
  const baseService = new BaseService();
  // const res = await baseService.post("/orders",context.query);
  // console.log(res['result']['_id']);
  return {
    props: {
      orderId : "123",
      restaurantId: context.query['restaurantId']
    },
  };
};
