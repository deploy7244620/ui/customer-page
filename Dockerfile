FROM node:18.18.0-alpine
WORKDIR /app
COPY package.json ./

RUN npm i
COPY . .
ENV TZ=Asia/Ho_Chi_Minh
RUN npm run build
CMD npm run start
EXPOSE 3000
